package se.skillarn.chat

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.Fuel
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import se.skillarn.chat.ui.login.LoginActivity

class NewSession {
    var token : String? = null
    var username : String? = null
    var errors : List<APIError>? = null
}

class Session {
    var username : String? = null
    var errors : List<APIError>? = null
}

class Me {
    var username : String? = null
}

class APIError {
    var value : String? = null
    var msg : String? = null
    var param : String? = null
    var location : String? = null
}

class NewMessage {
    var id : String? = null
    var content : String? = null
    var errors : List<APIError>? = null
}

class Messages {
    var messages : ArrayList<Message>? = null
    var errors : List<APIError>? = null
}

class Message {
    var content : String? = null
    var from : String? = null
    var id : String? = null
    var timestamp : String? = null
}

class ScrollingActivity : AppCompatActivity() {
    companion object {
        var session: NewSession? = null
        var loginUsername: String? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)
        setSupportActionBar(findViewById(R.id.toolbar))
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title
        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            Snackbar.make(view, "Creating new ", Snackbar.LENGTH_LONG).setAction("Action", null)
                .show()
        }
        if (session == null) {
            if (loginUsername == null) {
                val loginIntent = Intent(this@ScrollingActivity, LoginActivity::class.java)
                startActivity(loginIntent)
                return
            }
            Fuel.post("https://chat.dojnaz.net/api/me", listOf("username" to loginUsername!!))
                .response { _, _, result ->
                    val (bytes, error) = result
                    error?.printStackTrace()
                    if (bytes != null) {

                    }
                }
            return
        } else
            updateChat()
    }

    private fun updateChat() {
        if (session != null) {
            GlobalScope.launch(Dispatchers.IO) {
                val sessionGet = Fuel.get("https://chat.dojnaz.net/api/me")
                sessionGet.headers.append("x-chat-token", session!!.token!!)
                sessionGet.response { _, _, response ->
                    val (bytes, error) = response
                    error?.printStackTrace()
                    if (bytes != null) {
                        val me = Gson().fromJson(String(bytes), Me::class.java)

                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_scrolling, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}